@extends('layout.master')

@section('title')
Edit Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
@error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" value="{{$cast->umur}}" name="umur" class="form-control">
      
@error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
      <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection